FROM mcr.microsoft.com/dotnet/core/sdk:3.0

ADD ./get-pip.py ./

RUN apt-get update -y && apt-get install python3 python3-distutils -y
RUN curl --location --output /usr/local/bin/release-cli "https://release-cli-downloads.s3.amazonaws.com/latest/release-cli-linux-amd64"
RUN chmod +x /usr/local/bin/release-cli
RUN curl --location --output get-pip.py "https://bootstrap.pypa.io/get-pip.py"
RUN python3 get-pip.py
RUN pip install requests
